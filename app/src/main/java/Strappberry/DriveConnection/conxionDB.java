/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Strappberry.DriveConnection;

import java.io.FileInputStream;
import java.sql.*;
import java.util.Properties;
import javax.swing.JOptionPane;
/**
 *
 * @author ok25d
 */
public class conxionDB {
    Connection conn = null;
    Properties props = new Properties();
    
    public Connection conectar() throws SQLException{
            try { 
                String DRIVER = "com.ibm.as400.access.AS400JDBCDriver";
                String URL = "jdbc:as400://5.121.58.16;naming=system;errors=full";
                Connection conn = null;
               
                //Connect to iSeries 
                Class.forName(DRIVER);
                conn = DriverManager.getConnection(URL, "FORJAAPP", "APLIC4SECURE*");
                return conn;
            } catch (Exception e) {
                System.out.println( "conexion fallida: "+e.getLocalizedMessage().toString());
                return null;
            }
            
            
    }
    
    public void cerrar() throws SQLException{
        try{
            conn.close();
            JOptionPane.showMessageDialog(null,"desconexion exitosa", "conexion",JOptionPane.INFORMATION_MESSAGE);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "desconexion fallida"+e, "desconexion",JOptionPane.ERROR_MESSAGE);
        }
    }
}
